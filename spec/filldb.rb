# some users with authentications
puts "filldb.rb started..."
users = [
	{:email => 'user1@example.com'},
	{:email => 'user2@example.com'},
	{:email => 'user3@example.com', :score => 50}
]
count = 0
users.each do |u|
	count += 1 
	usr = User.new(u)
	usr.password = 'demodemo'
	usr.password_confirmation = 'demodemo'
	usr.save
	auth = Authentication.new :uid => count, :provider => 'demo'
	auth.user = usr
	auth.save
end
puts "Users inserted."
# some products

products = [
	{:barcode => "5410041435702", :name => 'Tuc Original - Salt'},
	{:barcode => "3800222850011", :name => 'Servetele de nas Nova Vita'},
	{:barcode => "5942325120016", :name => 'Apa plată Bilbor'}
]
products.each do |p|
	Product.create p
end
puts "Products inserted"


puts "Two properties created for barcode 03800222850011"
p = Product.find_by_barcode "3800222850011"
p.create_property(:description)
p.create_property(:tags)

puts "Now creating two opinions for 03800222850011:description"

prop = p.get_property(:description)
prop.addOpinion(User.find(:email => "user1@example.com"), {:value => "This is a pack of paper towels"})
prop.addOpinion(User.find(:email => "user2@example.com"), {:value => "This is a pack of paper towels that smells good and is pink."})

puts "done. Default locale used: #{I18n.locale}"