require 'spec_helper'

describe Opinion do
  it "should vote on opinion" do
  	p = Product.find_by_barcode "3800222850011"
  	property = p.get_property(:description)
  	user = User.find :email => 'user3@example.com'
  	opinion = property.opinions.find :first
  	opinion.vote(user).should be(true)
  end
end
