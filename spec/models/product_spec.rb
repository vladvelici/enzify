require 'spec_helper'

describe Product do
  it "should be created" do
  	p = Product.new :barcode => "5942355120016", :name => "TEST apa plata"
  	p.save.should be(true)
  end

  it "should be created by a user" do
  	p = Product.new :barcode => "5411041435702", :name => "TEST Tuc original"
  	p.author = User.create :email => 'product-author-test1@example.com', :password => 'test123', :password_confirmation => 'test123'
  	p.save.should be(true)
  	p.author.should be_an_instance_of(User)
  end

  it "should create and get a property :description, single value" do
    p = Product.find_by_barcode "5410041435702"
    p.create_property(:description).should be(true)
    p.get_property('description').should be_an_instance_of(Property)
  end

  it "should create and get a property :size, single value" do
    p = Product.find_by_barcode "5410041435702"
    p.create_property(:size).should be(true)
    p.get_property('size').should be_an_instance_of(SizeProperty)
  end

  it "should create and get a property :tags, multiple value" do
    p = Product.find_by_barcode "5410041435702"
    p.create_property(:tags).should be(true)
    p.get_property('tags').should be_an_instance_of(Property)
  end

  it "should return a property by name" do
    p = Product.find_by_barcode "3800222850011"
    p.get_property(:description).should be_an_instance_of(Property)
  end
end
