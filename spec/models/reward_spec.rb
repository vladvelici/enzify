require 'spec_helper'

describe Reward do
   it "should give some points to a user" do
  	user = User.new :email => 'reward-test-user@example.com', :password => 'something', :password_confirmation => 'something'
  	user.score = 5
  	user.save!
  	auth = User.new :email => 'reward-test-giver@example.com', :password => 'something', :password_confirmation => 'something'

    auth.save
    user.save

  	# testing the method
  	Reward.give(auth, user, 10).should be(true)

  	# testing the persisted user data
  	user = User.find :email => 'reward-test-user@example.com'
  	user.score.should eq(15)

	  # testing the persisted relationship
    user = User.find :email => 'reward-test-user@example.com'
  	auth = User.find :email => 'reward-test-giver@example.com'

  	reward_traverse = user.rels(:incoming, :rewards).to_other(auth)
    reward_traverse.count.should eq(1)

    reward = reward_traverse.first
    points = reward.points
#    pp reward.points
  	reward.points.should eq(10)
  end
end