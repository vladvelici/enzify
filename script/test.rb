def create_user_with_auth
	u = User.create :email => 'johny@example.com'
	a = Authentication.create :uid => 'naspa'
	b = Authentication.create :uid => 'fain'

	u.authenticate << a
	u.authenticate << b
end

def reward_give_test
	auth = Authentication.new :uid => 32542, :provider => 'something'
	puts "Authentication loaded."
	p auth
	user = User.new :email => "vlad@gmail.com"
	user.score = 5
	auth.save!
	user.save!(:validate => false)
	puts "User loaded\n"
	puts "User currently has a score of #{user.score} :) \n"
	final_score = user.score + 10
	reward = Reward.give auth, user, 10
	puts "Magic happened"
	p reward
	puts "now printing user.score for ... because it's cool \n it should be #{final_score}"
	p user.score

	puts "now checking for what really happened\n reinitializing user"
	user = User.find :email => 'vlad@gmail.com'
	auth = Authentication.find :uid => 32542, :provider => 'something'
	puts "printing user.score. also check it to be #{final_score}"
	p user.score
	puts "now checking if the relationship between these two exists"
	rew = user.rewards.first
	puts rew.points
end

reward_give_test
