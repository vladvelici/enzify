class Array
	def search(regex)
		out = []
		self.each do |s|
			out << s if s =~ regex
		end
		out
	end
end