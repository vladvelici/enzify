class Barcode
	def initialize(barcode)
		@code = barcode
		raise "Invalid barcode" unless validate
		@type = case @code.length
		when 6
			:upce
		when 8
			:ean8
		when 12
			:upca
		when 13
			:ean13
		when 14
			:gtin14
		end
	end

	def validate
		return false unless @code.respond_to? :to_s
		@code = @code.to_s
		return false unless [6,8,12,13,14].include? @code.length
		return false unless @code =~ /\A[0-9]+\Z/
		true
	end

	def self.standardize(barcode)
		begin
			code = self.new barcode
			code.gtin
		rescue
			false
		end
	end

	def gtin
		@gtin14 ||= to_gtin14
	end

	def originial_type
		@type
	end

	def original_code
		@code
	end

	def to_s
		gtin
	end

	def to_i
		gtin.to_i
	end

	private
	def to_gtin14
		case @type
		when :gtin14
			@code
		when :ean13
			from_ean13
		when :upca
			from_upca
		when :upce
			from_upce
		when :ean8
			from_ean8
		end
	end

	def from_ean13
		"0#{@code}"
	end

	def from_upca
		"00#{@code}"
	end

	def from_ean8
	    if @code =~ /^([0-9]{2})([0-9]{3})([0-2])$/ # XXNNN[0-2] -> 0XX[0-2]0000NNN
	      ean = "00#{$1}#{$3}0000#{$2}"
	    elsif @code =~ /^([0-9]{3})([0-9]{2})3$/ # XXXNN3 -> 0XXX00000NN
	      ean = "00#{$1}00000#{$2}"
	    elsif @code =~ /^([0-9]{4})([0-9])4$/ # XXXXN4 -> 0XXXX00000N
	      ean = "00#{$1}00000#{$2}"
	    elsif @code =~ /^([0-9]{5})([5-9])$/ # XXXXX[5-9] -> 0XXXXX0000[5-9]
	      ean = "00#{$1}00000#{$2}"
	    end
	    "0#{ean}" + ean_checkdigit(ean).to_s
	end

	def from_upce
		"000000#{@code}"
	end

	def ean_checkdigit(ean)
	    tmp = 1
	    sum = 0
	    ean.each_char do |c|
	      sum += tmp * c.to_i
	      if tmp == 1
	        tmp = 3
	      else
	        tmp = 1
	      end          
	    end
	    10 - (sum % 10)
	end
end