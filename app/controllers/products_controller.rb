class ProductsController < ApplicationController
  respond_to :json

  # GET /products
  # GET /products.json
  def index
    @products = Product.all
    respond_with @products
  end

  # GET /products/1
  # GET /products/1.json
  def show
    @product = Product.find_by_barcode params[:id]
    if @product
      respond_with @product.as_json({},true)
    else
      render :json => {:error => I18n.t('products.errors.not_found')}, :status => :not_found
    end
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new :name => params[:name], :barcode => params[:barcode]

    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, :notice => 'Product was successfully created.' }
        format.json { render :json => @product, :status => :created, :location => @product }
      else
        format.html { render :action => "new" }
        format.json { render :json => @product.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /products/1
  # PUT /products/1.json
  def update
    @product = Product.find(params[:id])

    respond_to do |format|
      if @product.update_attributes(params[:product])
        format.html { redirect_to @product, :notice => 'Product was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @product.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  # GET /search/:terms
  def search
    respond_with Product.search(params[:terms])
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product = Product.find(params[:id])
    if @product.destroy
      head :no_content
    else
      render :json => {:error => I18n.t('g.errors.paranoid')}
    end
  end
end