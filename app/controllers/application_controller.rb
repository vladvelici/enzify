include ApplicationHelper
class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :set_locale, :do_angular, :access_control
  # layout nil

  private
  def do_angular
  	# path = request.original_fullpath.split('/')
  	# fixing devise URLs without rewriting controllers
  	# is_devise = path[1] == 'users'

    render('layouts/angular', :layout => nil) if request.format == Mime::HTML && request.method == 'GET'
  end

  def set_locale
    unless params[:controller] == 'authentications' && params[:action] == 'create'
      subdomain_locale = request.subdomains.first
      if subdomain_locale && I18n.available_locales.include?(subdomain_locale.to_sym)
        I18n.locale = subdomain_locale
      elsif current_user && current_user.locale && I18n.available_locales.include?(current_user.locale.to_sym)
        redirect_to localized_path(current_user.locale)
      else
        redirect_to localized_path
      end
    end
  end
end
