class AuthenticationsController < ApplicationController
  # Skip rendering of the basic angular template on create
  before_filter :do_angular, :except => [:create]
  # before_filter :authenticate_user!, :only => [:index, :destroy]

  respond_to :json
  # GET /authentications
  # GET /authentications.json
  def index
    @authentications = current_user.authenticate
    respond_with @authentications
    # respond_to do |format|
    #   format.html # index.html.erb
    #   format.json { render :json => @authentications }
    # end
  end

  def create
    data = request.env['omniauth.auth']
    # render :text => data.to_yaml
    if auth = Authentication.find(:provider => data['provider'], :uid => data['uid'])
      sign_in_and_redirect :user, auth.user
    elsif user_signed_in?
      auth = Authentication.build_by_omniauth(data)
      current_user.authenticate << auth
      auth.save
      flash[:notice] = I18n.t('auth.notifications.success_add')
      redirect_to authentications_url
    else
      auth = Authentication.build_by_omniauth(data)
      if auth.email
        user = User.build_by_authentication(auth)
        # user.save!
        auth.save
        # user.confirm!
        flash[:notice] = I18n.t('devise.registrations.signed_up')
        sign_in_and_redirect :user, user
      else
        redirect_to new_user_registration_path, :alert => I18n.t('auth.notifications.no_email_provided')
      end
    end 
  end

  # DELETE /authentications/:id
  def destroy
    if current_user.authenticate.count > 1 || current_user.encrypted_password
      current_user.authenticate.find(params[:id]).destroy
      head :no_content
    else
      render :json => {"error" => I18n.t('auth.notifications.no_password_no_auth')}, :status => :conflict
    end
  end

end
