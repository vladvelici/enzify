class UsersController < ApplicationController

  before_filter :access_control, :except => [:me]

  respond_to :json

  def me
    user = current_user
    respond_with user
  end

  # def edit
  #   user = current_user
  #   if user.update_attributes(params[:user])
  #     head :no_content
  #   else
  #     render :json => @ingredient.errors, :status => :unprocessable_entity
  #   end
  # end

  def new_password
    if !params[:new_password] || !params[:new_password_confirm]
      redirect_to edit_user_registration_path, :alert => I18n.t('devise.passwords.new_invalid')
      return
    end
    user = current_user

    # do not allw password changing
    return redirect_to edit_user_registration_path, :alert => I18n.t('devise.passwords.new_invalid') if user.encrypted_password

    user.password = params[:new_password]
    user.password_confirmation = params[:new_password_confirm]
    if user.save
      redirect_to edit_user_registration_path, :notice => I18n.t('devise.passwords.updated_not_active')
    else
      redirect_to edit_user_registration_path, :alert => I18n.t('devise.passwords.new_invalid')
    end
  end
end