class OpinionsController < ApplicationController
	respond_to :json

	# POST /opinion/:id
	def vote
		@opinion = Opinion.find params[:id]
		user = current_user
		if @opinion.vote(user)
			render :json => {:score => @opinion.score}
		else
			render :json => {:error => I18n.t('g.errors.paranoid')}, :status => :unprocessable_entity
		end
	end

	# POST /opinion/:property
	def create
		@property = Property.find params[:property]
		unless @property
			render :json => {:error => I18n.t('g.errors.paranoid')}, :status => :unprocessable_entity
			return false
		end
		if opinion = @property.addOpinion(current_user, {:value => params[:opinion]})
			render :json => opinion
		else
			render :json => {:error => I18n.t('g.errors.paranoid')}, :status => :unprocessable_entity
		end		
	end

	def delete
		@opinion = Opinion.find params[:id]
		if @opinion.destroy
			head :no_content
		else
			render :json => {:error => I18n.t('g.errors.paranoid')}
		end
	end
end