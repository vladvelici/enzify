class PropertiesController < ApplicationController
	respond_to :json

	# POST /products/:id/create-property
	def create
		@product = Product.find(params[:id])
		if not @product
	    	render :json => {"error" => I18n.t('products.errors.not_found')}, :status => :unprocessable_entity
		elsif property = @product.create_property(params[:name].to_sym, current_user)
			render :json => property
		else
			render :json => {"error" => I18n.t('g.errors.paranoid')}, :status => :unprocessable_entity
		end
	end

	def delete
		@property = Property.find(params[:id])
		if @property.destroy
			head :no_content
		else
			render :json => {:error => I18n.t('g.errors.paranoid')}
		end
	end
end