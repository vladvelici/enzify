class SiteController < ApplicationController
  before_filter :do_angular, :except => [:test, :partials, :freescore]
  before_filter :access_control

  def index
  end

  def partials
  	partial = request.params[:partial].gsub('/', '')
  	partial.gsub!('..', '')

  	partial.gsub!(':','/')
 		partial = '/partials/' + partial

  	render partial, :layout => false
  end

  def freescore
    user = current_user
    user.score = 50
    user.save(:score)
    render :text => "you now have a score of 50pct..."
  end

  def test
    p = Product.find_by_barcode "5410041435702"
    # p.create_property(:description)
    p.trusted_properties
    render :text => 'mersă'
  end  
end
