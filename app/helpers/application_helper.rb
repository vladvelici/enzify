module ApplicationHelper
	def localized_path(locale=nil)
		locale = I18n.default_locale unless locale
		locale = locale.to_s unless locale.kind_of? String
		path = 'http://'+ locale  + '.' + request.host
		path += ':' + request.port.to_s unless request.port == 80
		path += request.fullpath
	end

	def access_control
		required = Reward.score_by_action([params[:controller],params[:action],'required']) || 0

		access = required == 0 ? true : false
		access = true if current_user && current_user.score >= required

		unless access
			# respond_to do |format|
		 #      format.html { render :status => :unauthorized, :view => '/errors/unauthorized' }
		 #      format.json { render :json => {:error => I18n.t('errors.unauthorized')}, :status => :unauthorized }
		 #    end
		 	render :json => {:error => I18n.t('g.errors.unauthorized')}, :status => :unauthorized
		end
	end
end