var ui = {
	toggleMenu: function() {
		$('#mobile-usermenu').toggle();
		$('#search').toggle();
	},
	toggleForm: function(form_id) {
		$(form_id).toggle();
		$(form_id + '-button').toggle();
	}
}