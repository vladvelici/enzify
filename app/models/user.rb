class User < Neo4j::Rails::Model
  property :name, :type => String
  property :score, :type => Fixnum, :default => 1
  property :locale, :type => String, :default => 'en'
  has_n(:authenticate).to(User)
  has_n(:rewards).relationship(Reward)
  has_n(:votes)
  has_n(:propsadded)
  has_n(:products)
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :name, :email, :password, :password_confirmation, :remember_me, :locale

  def display_name
  	name ? name : email
  end

  def self.build_by_authentication(auth)
  	if auth.email
  		# search user model by the provided email
  		user = User.find(:first, {:email => auth.email})
  		if !user
        if base_auth = Authentication.find(:first, {:email => auth.email})
    			user = base_auth.user
    		else
    			user = User.new
    			user.email = auth.email
        end
  		end
  		# user.skip_confirmation!
  	else
  		user = User.new
  	end
  	# set the user.name from data provided by Authentication
  	if !user.name
	    if auth.name
	    	user.name = auth.name
	    elsif auth.nickname
	    	user.name = auth.nickname
	    end
	end
    # link the Authentication to this user
    user.authenticate << auth
    user
  end

  def password_required?
  	(self.authenticate.empty? || !self.password.blank?) && super
  end

  # custom json repr for users
  def as_json(options={})
    repr = super options
    repr.merge!({
        'display_name' => self.display_name,
        'score' => self.score
      })
  end
end
