class Authentication < Neo4j::Rails::Model
  property :uid, :type => String, :index => :exact
  property :provider, :type => String, :index => :exact
  property :email, :type => String, :index => :exact
  property :name, :type => String
  property :nickname, :type => String
  property :picture, :type => String

  has_one(:user).from(User, :authenticate)

  after_create :pay_the_user
  before_destroy :charge_the_user

  def self.build_by_omniauth(omniauth)
  	if omniauth[:provider] and omniauth[:uid]
	    auth = self.new
	    auth.provider = omniauth[:provider] if omniauth[:provider]
	    auth.uid = omniauth[:uid] if omniauth[:uid]
	    if omniauth[:info]
	    	info = omniauth[:info]
	    	auth.email = info[:email] if info[:email]
	    	auth.picture = info[:image] if info[:image]
	    	auth.name = info[:name] if info[:name]
	    	auth.nickname = info[:nickname] if info[:nickname]
	    end
   		auth
	  end
  end

  private
  	def pay_the_user
 			Reward.give(self, self.user, 'authentications.create_new.reward')
  	end

    def charge_the_user
      Reward.substract(self, self.user)
    end
end
