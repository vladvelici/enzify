class Property < Neo4j::Rails::Model
	property :multiple_values, :type => :boolean, :default => false
	property :name, :index => :exact, :type => String
	property :locale, :index => :exact, :type => String, :default => nil
	property :has_value, :type => Fixnum, :index => :exact, :default => 0

	validates_presence_of :name

	after_create :do_rewarding
	before_destroy :fallback_rewarding
	before_save :make_locale_fit

	has_n(:opinions).to(Opinion)
	has_one(:product).from(Product, :properties)
	has_one(:author).from(User, :propsadded)

	# rewrite this according to what has to be stored for certain property types
	def addOpinion(user, hash)
		opinion = Opinion.new
		opinion.value = hash[:value]
		# example of using opinions with other types of things
		# opinion[:measure_unit] = unit
		save_opinion(user, opinion)
	end

	def value
		# unless self.multiple_values
		# 	opinions = self.opinions.all
		# 	node = nil
		# 	opinions.each do |o|
		# 		node = o if node == nil || o.score > node.score
		# 	end
		# 	if node
		# 		return [{:value => get_value(node), :score => node.score, :_nodeId => node.id}]
		# 	else
		# 		return []
		# 	end
		# else
			opinions = self.opinions.find(:all)
			values = []
			opinions.each do |op|
				values << {:value => get_value(op), :score => op.score, :_nodeId => op.id}
			end
			values
		# end
	end

	def as_json(options={})
		json = super options
		json.merge!({:values => value, :display_name => I18n.t("products.properties.#{self.name}")})
	end

	protected
		# the following method fit for TEXT properties
		# overwrite them to make other types of properties fit.
		def get_value(opinion)
			opinion.value
		end

	private
		def save_opinion(user, opinion)
			opinion.property = self
			opinion.author = user
			if opinion.save
				self.has_value = 1
				self.save(:has_value)
				opinion
			else
				nil
			end
		end

		def do_rewarding
			puts "REWARDING STARTS WTF"
			Reward.give(self, self.author, 'properties.create.reward')
		end

		def fallback_rewarding
			Reward.substract(self, self.author)
		end

		def make_locale_fit
			self.locale = 'not' unless self.locale
		end
end
