require 'lib/barcode.rb'
class Product < Neo4j::Rails::Model
  property :barcode, :type => String, :index => :exact, :unique => true
  # for searching by name
  property :name, :type => String, :index => :fulltext
  validates_presence_of :barcode, :name
  before_save :standardize_barcode

  attr_accessible :barcode, :name

  after_create :do_rewarding
  before_destroy :fallback_rewarding

  has_n(:properties).relationship(Properties)
  has_one(:author).from(User, :products)
  
  # Must return a hash with the properties, caring about the locale... also do processing to get key=>value
  def trusted_properties()
  	properties.all {|f| (f.locale == I18n.locale.to_s || f.locale == 'not') && f.has_value == 1}
  end

  # generates a JSON with all the properties available as trusted
  def as_json(options={}, properties = false)
    json_hash = super options
    return json_hash unless properties
    json_hash.merge!({:properties => get_properties.as_json})
  end

  def create_property(name, user=nil)
    # does it already exist?
    return false if get_property(name)
    # does it have a valid name (aka key)
    available_types = Product.property_keys
    prop = available_types[name]
    return false unless prop
    property = prop[:type].new
    property.multiple_values = prop[:multiple_values]
    property.name = name
    property.locale = I18n.locale if prop[:locale_dependent]
    property.product = self
    self.properties << property
    property.author = user if user
    self.save!
    property.save
    # property
    if property.save
      property
    else
      nil
    end
  end

  def self.property_keys
    {
      :description => {:type => Property, :multiple_values => false, :locale_dependent => true},
      :size => {:type => SizeProperty, :multiple_values => false, :locale_dependent => false},
      :tags => {:type => Property, :multiple_values => true, :locale_dependent => true},
      :ingredients => {:type => Property, :multiple_values => true, :locale_dependent => true},
      # 'photos' => {:type => PhotoProperty, :multiple_values => true}
    }
  end

  def self.find_by_barcode(barcode)
    barcode = Barcode::standardize barcode
    self.find :barcode => barcode
  end

  def get_property(name)
    properties.find {|f| f.name == name.to_s && (f.locale == I18n.locale.to_s || f.locale == 'not')}
  end

  def get_properties
    p I18n.locale

    properties.find_all {|f| f.locale == I18n.locale.to_s || f.locale == 'not'}
  end

  def self.search(terms)
    barcode = Barcode::standardize(terms)
    if barcode
      return [self.find_by_barcode(barcode)]
    else
      a = self.all({:name => terms}, :type=>:fulltext)
      return a if a
      return []
    end
  end

  private
    # save barcodes as GTIN
    def standardize_barcode
      self.barcode = Barcode.standardize barcode
    end

    def do_rewarding
      Reward.give(self, self.author, 'products.create.reward') if self.author
    end

    def fallback_rewarding
      Reward.substract(self, self.author) if self.author
    end
end
