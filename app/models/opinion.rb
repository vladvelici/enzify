class Opinion < Neo4j::Rails::Model
	property :value, :type => String
	property :score, :type => Fixnum, :default => 0, :index => :exact

	validates_presence_of :value
	has_one(:property).from(Property, :opinions)
	has_one(:author).from(User, :author_of)
	has_n(:users).from(User, :votes)

	after_create :do_rewarding
	before_destroy :fallback_rewarding

	def vote(user)
		return false if user.id == self.author.id
		return false if voted_on_opinion(user)
		# if allow_multiple_votes
			# if voted_on_opinion(user)
			# 	vote = self.rels(:incoming, :users).to_other(user)
			# 	if vote && vote = vote.first
			# 		vote.destroy
			# 		Reward.substract(self, user)
			# 		self.score -= 1
			# 		self.save(:score)
			# 	end
			# else
				self.users << user
				self.score += 1
				self.save
				Reward.give(self, user, 'opinions.vote.reward')
			# end
		# else
		# 	# if vote = voted_on_opinion(user) 
		# 	# 	vote.destroy
		# 	# 	Reward.substract(self, user)
		# 	# 	self.score -= 1
		# 	# 	self.save(:score)
		# 	# elsif 
		# 	opinion = voted_on_property(user)
		# 	vote_to_move = opinion.rels(:incoming, :users).to_other(user)
		# 	if vote_to_move && vote_to_move = vote_to_move.first
		# 		vote_to_move.destroy
		# 		opinion.score -= 1
		# 		opinion.save(:score)
		# 	else
		# 		self.users << user
		# 		self.score += 1
		# 		self.save
		# 		Reward.give(self, user, 'opinions.vote.reward')
		# 	end
		# end
	end

	def voted_on_opinion(user)
		return true if self.users.include? user
		false
		# if vote_trans = self.rels(:incoming, :users).to_other(user)
		# 	if vote_trans.respond_to? :rel_type
		# 		return vote_trans
		# 	else
		# 		return vote_trans.first
		# 	end
		# else
		# 	false
		# end
	end

	def voted_on_property(user)
		prop = self.property
		opinion = prop.opinions.find do |op|
			op.voted_on_opinion(user)
		end
		opinion
		# self.property.opinions.each do |opinion|
		# 	return opinion if opinion.voted_on_opinion(user)
		# end
	end

	# if this is true, multiple votes are allowed
	def allow_multiple_votes
		self.property && self.property.multiple_values
	end

	private
		def do_rewarding
			Reward.give(self, self.author, 'opinions.create.reward')
		end

		def fallback_rewarding
			Reward.substract(self, self.author)
		end
end
