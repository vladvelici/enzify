class SizeProperty < Property
	# rewrite this according to what has to be stored for certain property types
	def addOpinion(user, hash)
		opinion = Opinion.new
		opinion.value = hash[:value]
		opinion[:measure_unit] = hash[:unit]
		save_opinion(user, opinion)
	end

	protected
		def get_value(opinion)
			"#{opinion.value} #{opinion[:measure_unit]}"
		end
end