class Reward < Neo4j::Rails::Relationship
	property :points, :type => Fixnum
	property :action, :type => String

	@@rewardsHash = nil

	# NOTE: self.end_node is always a User object :)

	attr_accessible :points, :action

	before_destroy :substract_points

	def self.give(reference, user, action)
		points = self.score_by_action(action)
		return false unless user.kind_of? User
		return false unless points && points != 0
		return false if user.score+points < 0 # return false if the user can't pay *the bills*

		if existing_reward = user.rels(:incoming, :rewards).to_other(reference)
			if reward = existing_reward.first
				reward.points += points
				return false unless reward.save
			else
				reward = Reward.new :rewards, reference, user
				reward.points = points
				reward.action = action.kind_of?(Array) ? action.join('.') : action
			end
		else 
			reward = Reward.new :rewards, reference, user
			reward.points = points
			reward.action = action.kind_of?(Array) ? action.join('.') : action
		end
		unless reward.persisted?
			return false unless reward.save
		end
		user.score+=points
		user.save(:score)
	end

	def self.substract(reference, user)
		return false unless user.kind_of? User
		reward = user.rels(:incoming, :rewards).to_other(reference)
		if reward && reward = reward.first
		# 	user.score -= reward.points
		# 	user.save(:score)
			reward.destroy
		end
	end

	def self.score_by_action(action)
		return action if action.kind_of? Fixnum
		@@rewardsHash ||= YAML.load_file(Rails.root + 'config/rewarding_system.yml')
		result = hash_lookup(@@rewardsHash, action)
		if result.kind_of? Fixnum
			result
		else
			false
		end
	end

    def self.hash_lookup(hash, key)
      #make use of what code we can:
      key = I18n.normalize_keys(key,'','','.')
      # `stolen` from I18n
      key.inject(hash) do |result, _key|
        _key = _key.to_s # fixes incosistency: the YAML.load_file makes string keys
        return nil unless result.is_a?(Hash) && result.has_key?(_key)
        result = result[_key]
        result = resolve(locale, _key, result, options.merge(:scope => nil)) if result.is_a?(Symbol)
        result
      end
    end

	private
		def substract_points
			self.end_node.score -= self.points
			self.end_node.save(:score)
		end
end