class Neo4j::Rails::Model
	def as_json(options={})
		repr = super options
		repr.merge! '_nodeId' => self.id if self.persisted?
	end
end